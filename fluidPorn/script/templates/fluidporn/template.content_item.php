<? 
$link = generateUrl('video',$row['title'],$row['record_num']);
$dirname = str_replace('.flv','',$row[orig_filename]);
$subdir = $row[filename][0].'/'.$row[filename][1].'/'.$row[filename][2].'/'.$row[filename][3].'/'.$row[filename][4].'/'; 
$dirname = $subdir.$dirname; 
$uniq = uniqid(); 
?>
<div class="content <? echo $class; ?>">
    <div class="bg">
        <div class="img">
        <a href="<? echo $link; ?>">
		<? if($row[embed]) { ?>
            <img class="content_image" src="<? echo $thumb_url; ?>/embedded/<? echo $row[record_num]; ?>.jpg" alt="<? echo $row[title]; ?>"  width="<? echo $thumbwidth  ?>"  height="<? echo $thumbheight; ?>">
			<div class="overlay"></div>	
        <? } else { ?>
            <script type='text/javascript'>stat['<? echo $uniq; ?>']=0; pic['<? echo $uniq; ?>']=new Array(); pics['<? echo $uniq; ?>']=new Array(1,1,1,1,1,1,1,1,1,1);</script>
            <img class="content_image" src="<? echo $thumb_url; ?>/<? echo $dirname; ?>/<? echo $row[orig_filename]; ?>-<? echo $row[main_thumb]; ?>.jpg" alt="<? echo htmlentities($row[title]); ?>" id="<? echo $uniq; ?>" onmouseover='startm("<? echo $uniq; ?>","<? echo $thumb_url; ?>/<? echo $dirname; ?>/<? echo $row[orig_filename]; ?>-",".jpg");' onmouseout='endm("<? echo $uniq; ?>"); this.src="<? echo $thumb_url; ?>/<? echo $dirname; ?>/<? echo $row[orig_filename]; ?>-<? echo $row[main_thumb]; ?>.jpg";'  width="<? echo $thumbwidth  ?>"  height="<? echo $thumbheight; ?>">
			<div class="overlay"></div>
        <? } ?>        
		</a>        
        </div>	
        <div class="text">
            <h3><a href="<? echo $link; ?>"><? if($row['title']){ echo truncate($row['title'],20);} else {echo "Untitled";} ?></a></h3>
            <span class="rating">
				<span class="star_off"><span class="star_on"><? echo $row['rating']; ?>%</span></span>
			</span>       
        </div>
    </div>
</div>	