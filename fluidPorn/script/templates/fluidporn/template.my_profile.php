<!-- PROFILE-->
<div id="profile">
    <!-- general info -->
    <div class="general">
        <div class="picture">
            <? if($urow[avatar] != '') { ?>
                <img src='<? echo $basehttp; ?>/media/misc/<? echo $urow[avatar]; ?>' />
            <? } else { ?>
                <img src='<? echo $basehttp; ?>/core/images/avatar.jpg' />
            <? } ?>
        </div>

        <div class="info">
        	<h3>My Information</h3>
        	<span><strong>Username:</strong></span> <span><? echo $urow['username']; ?></span><br />
            <span><strong>Joined:</strong></span> <span><? echo date('F jS Y',strtotime($urow['date_joined'])); ?></span><br />
            <span><strong>Last Login:</strong></span> <span><? if($urow['lastlogin']) { echo date('Y-m-d \a\t H:i:s',$urow['lastlogin']); } else { echo 'Never'; } ?></span><br />
            <? if($urow['gender']) { ?>
            <span><strong>Gender:</strong></span> <span><? echo $urow['gender']; ?></span><br />
            <? } ?>
            <? if($urow['age']) { ?>
            <span><strong>Age:</strong></span> <span><? echo $urow['age']; ?></span><br />
            <? } ?>  
            <? if($urow['location']) { ?>
            <span><strong>Location:</strong></span> <span><? echo $urow['location']; ?></span><br />
            <? } ?>
      
            <? foreach($custom_user_fields as $k=>$v) { ?>
				<? if($custom[$k]) { ?>
                <strong><? echo $k; ?></strong>: <? echo htmlentities($custom[$k]); ?><br />
                <? } ?>
            <? } ?>  
                
            <br /><br />
            <? if($urow['description']) { ?>
                <h3>A little about me...</h3>
                <? echo nl2br($urow['description']); ?>
            <? } ?>
        </div>
    </div>
    <!-- / general info -->

    <!-- my comments -->
    <div class="my-comments row">
        <h4>My Comments <span class="more on"></span></h4>
		<div class="hidden" style="display:block;">
			<div class="list">
				<div id='ajaxComments'><!-- comments will display in here --></div>
			</div>
		</div>
    </div>
    <!-- / my comments -->

    <!-- my uploads -->
    <div class="my-uploads row">
        <h4>My Uploads <span class="more on"></span></h4>
		<div class="hidden" style="display:block;">
			<div class="navi">
				<a href="#tab1" class="active">Videos</a> | <a href="#tab2">Photos</a>
			</div>  
			<div class="list">
				<div id="tab1">
					<? userRecentUploads($_SESSION[userid],8); ?>
				</div>
				<div id="tab2" style="display: none;">
					<? userRecentUploads($_SESSION[userid],8,'photos'); ?>
				</div>
			</div>    
			<div class="more"><a href='<? echo $basehttp; ?>/uploads-by-user/<? echo $_SESSION[userid]; ?>/'>View All</a></div>
		</div>
    </div>
    <!-- / my uploads -->

    <div class="my-friends row">
        <h4>My Friends <span class="more on"></span></h4>
		<div class="hidden" style="display:block;">
			<div class="list">
				<? getUsersFriends($_SESSION['userid'],8); ?>
			</div>
		</div>
    </div>
</div>
<!-- / PROFILE -->

<!-- JS -->
<script type="text/javascript">
    $(document).ready(function(){
        $("#ajaxComments").load("<? echo $template_url; ?>/template.ajax_comments.php?id=<? echo $_SESSION['userid']; ?>&type=2&time=" + new Date().getTime());
		
        $('.my-uploads .navi').children().click(function(){
			$('.my-uploads .navi a').removeClass('active');
			$(this).addClass('active');			
            $('.my-uploads .list').children().hide();
            $($(this).attr('href')).show();
            return false;
        });
				
		
		
		$('#profile h4').click(function() {			
		  $(this).parent().children('.hidden').slideToggle('fast');
		  if($(this).children('.more').hasClass('off')){
		      $(this).children('.more').removeClass('off').addClass('on');
		  }else{
		  	  $(this).children('.more').removeClass('on').addClass('off');
		  }
		});	
		
		
		
		
		
		
		
							
		
    });
</script>
<!-- / JS -->