<!-- Panel -->
<div id="toppanel">
	<div id="panel">
		<div class="contentPanel clearfix">
			<div class="left" style="border:none;">
				<!-- Login Form -->
				<form class="clearfix" action="#" method="post" id="LoginForm">
					<h1>Member Login</h1>
                    <div class="ajaxInfo"></div>	
                    <div class="loader"></div>
					<label class="grey" for="log">Username:</label>
					<input class="field" type="text" name="username" id="log" value="" size="23" />
					<label class="grey" for="pwd">Password:</label>
					<input class="field" type="password" name="password" id="pwd" size="23" />
        			<div class="clear"></div>
					<a href="#" class="mbButton" id="loginAccountButton">Login</a><br>
				</form>
			</div>
			<div class="left right">			
				<!-- Register Form -->
				<form action="#" method="post" id="SignupForm">
					<h1>Not a member yet? Sign Up!</h1>	
                    <div class="ajaxInfo"></div>	
                    <div class="loader"></div>  
                    <div id="statusSignup">                  	
                    <div class="column">
                        <label class="grey" for="signup_username">Username:</label>
                        <input class="field" type="text" name="signup_username" id="signup" value="" size="23" />
                        <label class="grey" for="signup_password">Password:</label>
                        <input class="field" type="text" name="signup_password" id="signup_password" size="23" />
                        <label><a href="#" class="mbButton" id="createAccountButton">Create Account!</a></label>
                    </div>
                    <div class="column">
                        <label class="grey" for="signup_email">Email:</label>
                        <input class="field" type="text" name="signup_email" id="email" size="23" /> 
                        <? if($enable_signup_captcha) { ?>
                        <label class="grey" for="captchaaa">Prove you're human:</label>
                        <label class="captcha">
                            <input class="field" type="text"  name="captchaaa" style="width:138px;"><img src="<? echo $basehttp; ?>/captchaBlack.php">   
                        </label>
                        <? } ?>
                        <label>
						<? if(!$_SESSION[userid] && $enable_facebook_login){ ?>
                        <?php include($basepath.'/facebook_login.php'); ?>					
                            <a href="<? echo $basehttp; ?>/includes/facebook/facebook.php" style="margin-top:10px; display:block; float:right; margin-right:8px;"><img src="<?php echo $basehttp;?>/core/images/facebook-login-button.png" alt="Login by FaceBook" border="0" /></a>
                        <? } ?>   
						<? if(!$_SESSION[userid] && $enable_twitter_login){ ?>
                            <a href="<?php echo $twitter->getAuthenticateUrl(); ?>&force_login=true" style="margin-top:10px; display:block; float:left;">
                                <img src="<?php echo $basehttp;?>/core/images/twitter-login-button.png" alt="Login by Twitter" />
                            </a>
                    	<? } ?>                      
                        </label>                        
                    </div>                        
                    </div>
                 </form>
			</div>
		</div>
</div> <!-- /login -->	

	<div class="tabPanel">
	<div class="contener">
		<ul class="login">
			<li class="left">&nbsp;</li>
            <? if($_SESSION['username']){ ?>
			<li>Logged in as:  <strong><? echo $_SESSION['username']; ?></strong></li>
            <? } ?>
			<li class="sep">|</li>
			<li id="toggle">
                <? if(!$_SESSION['userid']){ ?>
				<a id="open" class="open" href="#"><strong>Log In</strong> <span>|</span> Register</a>
                <? } else { ?>
                <a id="open" class="open" href="#">User Panel</a>
                <? } ?>
				<a id="close" style="display: none;" class="close" href="#">Close Panel</a>			
			</li>
			<li class="right">&nbsp;</li>
		</ul>
	</div>	 
	</div>
	
</div> <!--panel -->



<script type="text/javascript">

	$(document).ready(function(){	
	
	
	$('#LoginForm').find('input').each(function(){
		$(this).keypress(function(e){
			if(e.which == 13){
				sendFormLogin();
			}
		});
	});	
	$('#loginAccountButton').bind('click',function(){
		sendFormLogin();					
		return false;
	});	
		
	$('#SignupForm').find('input').each(function(){
		$(this).keypress(function(e){
			if(e.which == 13){
				sendFormSignup();		
			}
		});
	});	
	$('#createAccountButton').bind('click',function(){
		sendFormSignup();					
		return false;
	})		
		
	function sendFormLogin(){
		$('#LoginForm .loader').show();
		$.post('<? echo $basehttp; ?>/includes/ajax.login.php',{post: $('#LoginForm').serialize()},function(data){
			$('#LoginForm .loader').hide();
			data.message = $.base64.decode(data.message);
			if(data.error == 'true') {
				 $('#LoginForm .ajaxInfo').html(data.message);						 
			} else {
				 $('#LoginForm .ajaxInfo').html(data.message);
				 $('#LoginForm .loader').hide();
				 window.location.href='<? echo $basehttp.$_SERVER['REQUEST_URI']; ?>';
			}
		},'json');	
	};		
	
	function sendFormSignup(){
		$('#SignupForm .loader').show();
		$.post('<? echo $basehttp; ?>/includes/ajax.signup.php',{post: $('#SignupForm').serialize()},function(data){
			$('#SignupForm .loader').hide();
			data.message = $.base64.decode(data.message);
			if(data.error == 'true') {
				 $('#SignupForm .ajaxInfo').html(data.message);						 
			} else {
				 $('#SignupForm .ajaxInfo').html(data.message);
				 $('#statusSignup').hide();
			}
		},'json');	
	};	



	
	

		

		
	});
</script> 