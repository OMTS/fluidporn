<div class="forms-wrapper">
    <div class="forms">
    		
		<? if(count($errors) > 0){ ?>
        <div class="notification error"><p>	
            <? foreach($errors as $r){ ?>
         		&bull; <? echo $r; ?><br />
            <? } ?>
        </p></div>    
        <? } ?>

		<? if($_GET['success']){ ?>
		<div class="notification success"><p>Your <? echo $optionTitle; ?> has been submitted, and will appear on the site shortly pending moderator approval.</p></div>
		<? } else { ?>
		<div class="notification info">
		<p>
		<ol>
			<li>I certify that all models depicted are at least <strong>18 years of age</strong>.</li>
			<li>I certify that <strong>I am the owner </strong>or licensed to use this content.</li>
			<li>I certify that <strong>I have consent</strong> of all models depicted to submit this content.</li>			
		</ol>
		</p>	
		<p>You are logged in as <strong><? echo $_SESSION[username]; ?></strong> and your IP is <strong><? echo $_SERVER['REMOTE_ADDR']; ?></strong></p>
		</div>	
        
        <form id="CaptchaForm"  method="post" action="javascript:void();">
        <div class="ajaxInfo"></div>
        <div class="loader" style="display:none;">loading...</div>
		<table border="0">
			<tr>
				<td width="120">Captcha</td>
				<td>
                	<img src="<? echo $basehttp; ?>/captcha.php">       				
				</td>
			</tr>
            <tr>
            	<td>Human ?</td>
                <td><input name="captchaaa" type="text" id="captchaUpload"  style="min-width:158px;" maxlength="255" /></td>
            </tr>			
			<tr>
				<td>&nbsp;</td>
				<td>
					<input type="submit" name="button" id="btnCaptcha" value="OK" />
				</td>
			</tr>  
		</table>    
        </form>

		<form name="formUpload" id="formUpload" method="post" action="" style="display:none;">	
		<input type="hidden" name="hidFileID" id="hidFileID" />	
		<table id="upload-table" border="0">
			<tr>
				<td style="border:none; padding:0px;"></td>
				<td style="border:none; padding:0px;"><input type="text" id="txtFileName" disabled="true" /></td>
			</tr>
			<tr>
				<td><strong>File</strong></td>
				<td>
                	<div id="uploadStatus" style="clear:both;"></div>
                	<div id="fine-uploader" style="padding:5px; padding-left:0px;"></div>                				
				</td>
			</tr>
			<tr>
				<td><strong>Title</strong></td>
				<td><input name="title" type="text" id="titleUpload"  class="validate[required,maxSize[60]]" maxlength="255" /></td>
			</tr>
			<tr>
				<td><strong>Description</strong></td>
				<td><textarea  name="description" id="descriptionUpload"  maxlength="255" class="validate[required,maxSize[255]]"></textarea></td>
			</tr>
			<tr>
				<td><strong>Categories</strong></td>
				<td>
				<? 
				$result = dbQuery("SELECT name, record_num FROM niches ORDER BY name ASC", false);
				foreach($result as $srow) {
					$categoriesDrop[] = "<label><input type='checkbox' class='validate[minCheckbox[1],maxCheckbox[3]] channelUpload' name='channels[]' id='channel$srow[record_num]' value='$srow[record_num]'  />$srow[name]</label><br />";
				}
				$categoriesChunked = array_chunk($categoriesDrop,ceil(count($categoriesDrop)/4));	
				foreach($categoriesChunked as $z) {
				?>
					<div style='width: 23%; margin-right: 10px; float: left;'>
						<? 
						foreach($z as $i) {
							echo $i;
						}
						?>
					</div>
				   <?
				}
				?> 	
				</td>
			</tr>
			<tr>
				<td><strong>Tags</strong></td>
				<td><input name="tags" type="text" id="tagsUpload" maxlength="255" class="validate[required,maxSize[60]]" /></td>
			</tr>				
			<tr>
				<td>&nbsp;</td>
				<td>
					<input type="submit" name="button" id="btnSubmit" value="Upload now !" />
				</td>
			</tr>  
		</table>
		</form>
		<? } ?>
    </div>
</div>