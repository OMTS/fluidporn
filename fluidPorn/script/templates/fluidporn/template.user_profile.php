<!-- PROFILE-->
<div id="profile">
    <!-- general info -->
    <div class="general">
        <div class="picture">
            <? if($urow[avatar] != '') { ?>
                <img src='<? echo $basehttp; ?>/media/misc/<? echo $urow[avatar]; ?>' />
            <? } else { ?>
                <img src='<? echo $basehttp; ?>/core/images/avatar.jpg' />
            <? } ?>
			
            <? if($_SESSION['userid'] && ($urow[record_num] !== $_SESSION['userid'])) { ?>          
            	<a href="<? echo $basehttp; ?>/includes/inc.add_friend.php?option=add&id=<? echo $urow['record_num']; ?>" id="addAsFriend">Add <? echo ucwords($urow['username']); ?> To Friends</a> 
            <? } ?>		
        </div>

        <div class="info">
            <h3>User Information</h3>
        	<span><strong>Username:</strong></span> <span><? echo $urow['username']; ?></span><br />
            <span><strong>Joined:</strong></span> <span><? echo date('F jS Y',strtotime($urow['date_joined'])); ?></span><br />
            <span><strong>Last Login:</strong></span> <span><? if($urow['lastlogin']) { echo date('Y-m-d \a\t H:i:s',$urow['lastlogin']); } else { echo 'Never'; } ?></span><br />
            <? if($urow['gender']) { ?>
            <span><strong>Gender:</strong></span> <span><? echo $urow['gender']; ?></span><br />
            <? } ?>
            <? if($urow['age']) { ?>
            <span><strong>Age:</strong></span> <span><? echo $urow['age']; ?></span><br />
            <? } ?>  
            <? if($urow['location']) { ?>
            <span><strong>Location:</strong></span> <span><? echo $urow['location']; ?></span><br />
            <? } ?>
      
            <? foreach($custom_user_fields as $k=>$v) { ?>
				<? if($custom[$k]) { ?>
                <strong><? echo $k; ?></strong>: <? echo htmlentities($custom[$k]); ?><br />
                <? } ?>
            <? } ?>  
                
            <br /><br />
            <? if($urow['description']) { ?>
                <h3>A little about me...</h3>
                <? echo nl2br($urow['description']); ?>
            <? } ?>
        </div>
    </div>
    <!-- / general info -->
    	
	

    <!-- my comments -->
    <div  class="send-message row forms-wrapper">
        <h4>Send Private Message <span class="more off"></span></h4>
		<div class="hidden" <? if($success || $error) echo 'style="display:block"';?>>        
		<? if($success) echo '<div class="notification success">'.$success.'</div>'; ?>
		<? if($error) echo '<div class="notification error">'.$error.'</div>'; ?>
        <? if(!$_SESSION['userid']) { ?>
        <p>You must be logged in to send messages. Please <a href='<? echo $basehttp; ?>/login'>login</a> or <a href='<? echo $basehttp; ?>/signup'>signup (free)</a></p>
        <? } else { ?>
			<? if(!$success && !$error) { ?>
            <div class="send-message-form">      
                <form method="post" action="">
                    <strong>Subject: </strong><br />
                    <input name="subject"type="text" /><br />
                    <strong>Your Message:</strong><br />
                    <textarea name="text" cols="" rows=""></textarea><br />
                    <input name="send" type="submit" value="Send Message" />
                </form>				 
            </div>
            <? } ?>
        <? } ?>
		</div>
    </div>
    <!-- / my comments -->
    
    <!-- my uploads -->
    <div class="my-uploads row">
        <h4><? echo ucwords($urow['username']); ?>'s Recent Uploads <span class="more on"></span></h4>
		<div class="hidden" style="display:block;">
			<div class="navi">
				<a href="#tab1" class="active">Videos</a> | <a href="#tab2">Photos</a>
			</div>  
			<div class="list">
				<div id="tab1">
					<? userRecentUploads($id,8); ?>
				</div>
				<div id="tab2" style="display: none;">
					<? userRecentUploads($id,8,'photos'); ?>
				</div>
			</div>    
			<div class="more"><a href='<? echo $basehttp; ?>/uploads-by-user/<? echo $urow['record_num']; ?>/'>View All</a></div>
		</div>
    </div>
    <!-- / my uploads -->
    
    <div class="my-friends row">
        <h4><? echo $profusername; ?>'s Friends<span class="more on"></span></h4>
		<div class="hidden" style="display:block;">
			<div class="list">
				<? getUsersFriends($urow['record_num'],8); ?>
			</div>
		</div>
    </div>    
    

    <div class="my-friends row">
        <h4><? echo $profusername; ?>'s Wall<span class="more on"></span></h4>
		<div class="hidden" style="display:block;">
        <div class="list forms-wrapper">        
 			<? 
				$contentID = $urow['record_num']; 
				$commentsType = 2; 
				include($basepath.'/includes/inc.comments.php');
			?>  
        </div>
		</div>
    </div>
</div>
<!-- / PROFILE -->