<div id="profile" class="my_profile">
    
<div class="notification info">
	<? $resultNew = dbQuery("SELECT  friends.record_num as friend_id, friends.friend_request_message, friends.date_added,  users.record_num, users.username FROM friends, users 
		WHERE  friends.friend = '$_SESSION[userid]' AND users.record_num =  friends.user AND approved = 0 ORDER BY friends.date_added",false); ?>
    <p>You have (<strong><? echo count($resultNew); ?></strong>) new friend request(s).</p>
</div>
    
    
<div class="row">
<h4>New invitation <span class="more on"></span></h4>
<div class="hidden" style="display:block;">
<form method="post" action="">
<table width="100%" border="0" class="sTable">
    <thead>
    <tr>
        <td align="center" width="150"><div align="left">User</div></td>
        <td align="center"><div align="left">Message</div></td>
        <td align="center"><div align="left">Approve</div></td>	
        <td align="center"  width="90"><div align="left">Sent</div></td>
        <td align="center"  width="60"><div align="center">Delete</div></td>
    </tr>	
    </thead>
    <tbody>
    <?
    if($resultNew){
        foreach($resultNew as $nrow){
            $link=generateUrl('user',$nrow[username],$nrow[record_num]);
    ?>
    <tr id="friend<? echo $nrow[friend_id]; ?>">
        <td><a href="<? echo $link; ?>"><? echo $nrow[username]; ?></a></td>
        <td><? echo $nrow[friend_request_message]; ?></td>
        <td><div align="center"><a onClick="javascript:approveFriend('<? echo $nrow[friend_id]; ?>'); return false;" href="#" class="addIco"></div></td>
        <td><strong><? echo datediff('',$nrow[date_added],date('Y-m-d H:i:s'),false); ?></strong> ago</td>        
        <td align="center"><div align="center"><a onClick="javascript:deleteFriend('<? echo $nrow[friend_id]; ?>'); return false;" href="#" class="removeIco"></a></div></td>		
    </tr>
    <?
        } ?>				
 <td colspan="5"> <p><a href="<? echo $basehttp; ?>/my-friends?acceptAll=1">Accept ALL</a> /  <a href="<? echo $basehttp; ?>/my-friends?declineAll=1">Decline  ALL</a></a></td>	
	<?	} else {
  ?>
  <td colspan="5">No new invation</td>
  <? } ?>
</tbody>
</table>
</form>
</div>  
</div>

  
  
<div class="row">
<h4>My Friends<span class="more on"></span></h4>
<div class="hidden" style="display:block;">
<form method="post" action="">
<table width="100%" border="0" class="sTable">
    <thead>
    <tr>
        <td align="center" width="150"><div align="left"><a href="?sortby=name">User</a></div></td>
        <td align="center"><div align="left"><a href="?sortby=date">Date added</a></div></td>			
        <td align="center" width="60"><div align="center">Delete</div></td>
    </tr>	
    </thead>
    <tbody>
    <?
    
    if(isset($_GET[sortby]) && $_GET[sortby] == 'name'){
        $orderBy = ' users.username ASC ';
    }else if(isset($_GET[sortby]) && $_GET[sortby] == 'date'){
        $orderBy = ' friends.date_added DESC ';
    }else{
        $orderBy = 'friends.date_added DESC';
    }	    
    $resultFriends = dbQuery("SELECT  friends.record_num as friend_id, friends.friend_request_message, friends.date_added,  users.record_num, users.username FROM friends, users WHERE  friends.friend = '$_SESSION[userid]' AND users.record_num =  friends.user AND approved = 1 ORDER BY $orderBy",false);		
    
    if($resultFriends){
        foreach($resultFriends as $frow){
            $link=generateUrl('user',$frow[username],$frow[record_num]);
    ?>
    <tr id="friend<? echo $frow[friend_id]; ?>">
        <td><a href="<? echo $link; ?>"><? echo $frow[username]; ?></a></td>			
        <td><strong><? echo datediff('',$frow[date_added],date('Y-m-d H:i:s'),false); ?></strong> ago</td>
        <td align="center"><div align="center"><a onClick="javascript:deleteFriendApproved('<? echo $frow[friend_id]; ?>'); return false;" class="removeIco" href="#"></a></div></td>		
    </tr>
    <?
        }	
    
    } else {
  ?>
  <td colspan="3">You have no friends.</td>
  <? } ?>
</tbody>
</table>
</form>
</div>
</div>

  
  
<div class="row">
<h4>Sent invitation<span class="more on"></span></h4>
<div class="hidden" style="display:block;">
<form method="post" action="">
<table width="100%" border="0" class="sTable">
    <thead>
    <tr>
        <td align="center" width="150"><div align="left">User</div></td>	
        <td align="center"><div align="left">Message</div></td>		
        <td align="center" width="90"><div align="left">Sent</div></td>			
        <td align="center" width="60"><div align="center">Delete</div></td>
    </tr>	
    </thead>
    <tbody>
    <?
    $resultSent = dbQuery("SELECT  friends.record_num as friend_id, friends.friend_request_message, friends.date_added, users.record_num, users.username FROM friends, users WHERE  friends.user = '$_SESSION[userid]' AND users.record_num =  friends.friend AND  friends.approved = 0  ORDER BY friends.date_added DESC",false);
    if($resultSent){
        foreach($resultSent as $srow){
            $link=generateUrl('user',$srow[username],$srow[record_num]);
    ?>
    <tr id="friend<? echo $srow[friend_id]; ?>">
        <td><a href="<? echo $link; ?>"><? echo $srow[username]; ?></a></td>
        <td><? echo $srow[friend_request_message ]; ?></td>
        <td align="center"><div align="left"><strong><? echo datediff('',$srow[date_added],date('Y-m-d H:i:s'),false); ?></strong> ago</div></td>	
        <td align="center"><div align="center"><a onclick="javascript:deleteFriend('<? echo $srow[friend_id]; ?>'); return false;" href="#" class="removeIco"></a></div></td>		
    </tr>
    <?
        }	
    
    } else {
  ?>
  <td colspan="4">No result</td>
  <? } ?>
</tbody>
</table>
</form>
</div>
</div>



</div>