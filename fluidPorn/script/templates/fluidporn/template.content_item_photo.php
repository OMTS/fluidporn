<? 
$link = generateUrl('galleries',$row['title'],$row['record_num']);
$dirname = str_replace('.flv','',$row[orig_filename]);
$subdir = $row[filename][0].'/'.$row[filename][1].'/'.$row[filename][2].'/'.$row[filename][3].'/'.$row[filename][4].'/'; 
$dirname = $subdir.$dirname; 
if(!$row[thumbfile]) {
	$sresult = mysql_query("SELECT filename FROM images WHERE record_num = '$row[thumbnail]'");
	$srow = mysql_fetch_assoc($sresult);
	$row[thumbfile] = $srow[filename];
}
?>
<div class="content <? echo $class; ?>">
    <div class="bg">
        <div class="img">
            <a href="<? echo $link; ?>" target="_self">
                <img class="content_image" src="<? echo $gallery_url; ?>/<? echo $row[filename]; ?>/thumbs/<? echo $row[thumbfile]; ?>" alt="<? echo $row[title]; ?>" width="<? echo $picthumbwidth  ?>"  height="<? echo $picthumbheight; ?>">
				<div class="overlay"></div>	
			</a>
        </div>	
        <div class="text">
            <h3><a href="<? echo $link; ?>"><? echo truncate($row['title'],15); ?></a></h3>
            <span class="rating">
				<span class="star_off"><span class="star_on"><? echo $row['rating']; ?>%</span></span>
			</span>  
			 
        </div>
    </div>
</div>	