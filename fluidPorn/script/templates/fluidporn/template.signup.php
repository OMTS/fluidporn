<div class="forms-wrapper">
    <div class="forms">
	  	<? if(!$errors && $_GET['done'] !== 'true') { ?>	
        <div class="notification info">
            <p>Sign up for a personal account to save videos, leave comments and utilize other advanced features!</p>          
        </div>
		<? } ?>
		
        <? if($errors) { ?>
		<div class="notification error">
		<p>
			<strong>The following errors have occured:</strong><br>
			<? 
				foreach($errors as $i) {
					echo "&bull; $i<br>";
				} 
			?>
		</p>	
		</div>
        <? } ?>
		
        <? if(isset($_GET[done]) && $_GET[done] == 'true'){ ?>
        <div class="notification success">
           <p>
                <? if($require_account_confirmation){ ?>
                Verification mail was sent to you email address.<br />
                To login please <strong>verify your account</strong> through email verification link.
                <? } else { ?>
                You may now login at the <a href="<? echo $basehttp; ?>/login">login page</a>
                <? } ?>
            </p>
        </div>
        <? } else {  ?>				
        <form  name="signupForm" method="post" action="">
            <div class="form-label">Username</div>
            <div class="form-field"><input id="signup_username" name="signup_username" type="text" maxlength="25" value="<? echo $thisusername; ?>" style="width:300px;" /></div>
			
            <div class="form-label">Password</div>
            <div class="form-field"><input id="signup_password" name="signup_password"  type="password" maxlength="35" value="" style="width:300px;" /></div>
			
            <div class="form-label">E-mail</div>
            <div class="form-field"><input id="signup_email" name="signup_email" type="text" maxlength="35" value="<? echo $thisemail; ?>" style="width:300px;" /></div>		
			
			<? if($enable_signup_captcha) { ?>		
			<div class="form-label">Human ?</div>			
			<div class="form-field">
				<img src="<? echo $basehttp; ?>/captcha.php?<? echo time(); ?>" class="captcha"><br />
				<input id="signup_email" name="captchaaa" type="text" size="10" value="" style="width:168px; margin-top:5px;">
			</div>
			<? } ?>			
					
            <div class="form-field"><input class="submit" type="submit" name="Submit" id="button" value="Register" /></div>
			<? if(!$_SESSION[userid] && $enable_facebook_login){ ?>
            <?php include($basepath.'/facebook_login.php'); ?>					
                 <div class="form-field" style="margin-top:10px;  float:left;"><a href="<? echo $basehttp; ?>/includes/facebook/facebook.php"><img src="<?php echo $basehttp;?>/core/images/facebook-login-button.png" alt="Login by FaceBook" border="0" /></a></div>   
			<? if(!$_SESSION[userid] && $enable_twitter_login){ ?>
            	<div class="form-field" style="margin-top:10px; float:left;">
                <a href="<?php echo $twitter->getAuthenticateUrl(); ?>&force_login=true"><img src="<?php echo $basehttp;?>/core/images/twitter-login-button.png" alt="Login by Twitter" /></a></div>
            <? } ?>                     
                 
            <? } ?>   
 
        </form>
		<? } ?>
    </div>
</div>		   