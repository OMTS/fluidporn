<? 
$link = generateUrl('channel',$row['name'],$row['record_num']);
?>

<div class="content content-channel <? echo $class; ?>">
    <div class="bg">
        <div class="img">			
            <a href="<? echo $link; ?>">
            <? if(file_exists($misc_path.'/cat'.$row[record_num].'.jpg')) { ?>
                <img class="content_image" src="<? echo $misc_url; ?>/cat<? echo $row[record_num]; ?>.jpg" alt="<? echo $row[name]; ?>"   width="<? echo $thumbwidth  ?>">
				<div class="overlay"></div>	
            <? } else { ?>
                <img class="content_image" src="<? echo $basehttp; ?>/core/images/catdefault.jpg" alt="<? echo $row[name]; ?>"   width="<? echo $thumbwidth  ?>">
				<div class="overlay"></div>	
            <? } ?>
            </a>
		</div>
        <div class="text">
            <h3><a href="<? echo $link; ?>"><? echo ucwords($row['name']); ?></a> <small>(<? echo $row['total']; ?>)</small></h3>
        </div>
    </div>
</div>