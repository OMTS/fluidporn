<!doctype html>
<html>
  <head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title><? echo $title; ?> at <? echo $sitename; ?></title>
	<meta name="keywords" content="<? echo $metakeywords; ?>" />
	<meta name="description" content="<? echo $metadescription; ?>" />
	 <?
		if($rrow['photos'] == 1){
			$link = generateUrl('galleries',$rrow['title'],$rrow['record_num']);
		} else {
			$link = generateUrl('video',$rrow['title'],$rrow['record_num']);
		}
	?>
    <meta name="viewport" content="width=device-width, initial-scale=1, minimal-ui">
    <link rel="canonical" href="<? getVideoLink($rrow);  ; ?>" />
	<link rel="canonical" href="<? echo $link; ?>" />
    <link type="text/css" href="<? echo $basehttp; ?>/core/css/base.css" rel="stylesheet" />
	<link type="text/css" href="<? echo $template_url; ?>/css/normalize.css" rel="stylesheet" />
	<link type="text/css" href="<? echo $template_url; ?>/css/style.css" rel="stylesheet" />
	<link type="text/css" href="<? echo $template_url; ?>/js/uniform/css/uniform.default.css" rel="stylesheet" />
	<link type="text/css" href="<? echo $template_url; ?>/js/cbox/colorbox.css" rel="stylesheet" />
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
	<script type="text/javascript">
		var _basehttp = '<? echo $basehttp; ?>';
	</script>
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js"></script>
	<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"></script>
	<script type="text/javascript" src="<? echo $template_url; ?>/js/uniform/jquery.uniform.min.js"></script>
    <script src="<? echo $basehttp; ?>/includes/player/flowplayer-3.2.11.min.js"></script>
    <script type="text/javascript" src="<? echo $basehttp; ?>/core/js/thumbchange.js"></script>	
	<script type="text/javascript" src="<? echo $template_url; ?>/js/template.function.js"></script>
	<script type="text/javascript" src="<? echo $template_url; ?>/js/cbox/jquery.colorbox-min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
             
            
            $('#reportContent').bind('click',function(){
                var link = $(this).attr('href');
                $.colorbox({
                    iframe: true,
                    href: link,
                    width: '550px',
                    height: '460px'
                });
                return false;
            });
            
            $('#addToFavs').bind('click',function(){
                var link = $(this).attr('href');
                $.colorbox({
                    href: link
                });
                return false;
            });
			
			 
            

			$('.menu-button').click(function(){
				$('#navmenu').slideToggle("fast");
			 });
			 
			 $('#search-slide').click(function(){
				$('#search-container').show();
			 });
			 
			 $("#search-container").mouseleave(function() {
				 
				setTimeout(function() {$("#search-container").hide();}, 5000);
			});
			 
			$( "#profile-slide" ).click(function() {
			    $( "#login-container").toggle();
			});
			$("ul.login").mouseleave(function() {
				 
				setTimeout(function() {$( "#login-container").hide();}, 1000);
			});

			$( "ul#search-list" ).hover(
				function() {
					$( this ).css('overflow','visible');
				}, function() {
					$( this ).css('overflow','hidden');
				}
			);
      
			
        });
    </script>
</head>
<body>
    <? getTemplate('template.nav.php'); ?>
	<section id="content">
		<div class="container">