<div>
    <p>In accordance with the Digital Millennium Copyright Act of 1998 (the text of which may be found on the U.S. Copyright Office website at http://lcweb.loc.gov/copyright/), <? echo $sitename; ?> will respond expeditiously to claims of copyright infringement that are reported to <? echo $sitename; ?>'s designated copyright agent identified below. Please also note that under Section 512(f) any person who knowingly materially misrepresents that material or activity is infringing may be subject to liability. <? echo $sitename; ?> reserves the right at its sole and entire discretion, to remove content and terminate the accounts of <? echo $sitename; ?> users who infringe, or appear to infringe, the intellectual property or other rights of third parties.</p>
<p>If you believe that your copywriten work has been copied in a way that constitutes copyright infringement, please provide <? echo $sitename; ?>'s copyright agent the following information:</p>
<p>1.     A physical or electronic signature of a person authorized to act on behalf of the owner of an exclusive right that is allegedly infringed;</p>
<p>2.     Identification of the copyright work claimed to have been infringed, or, if multiple copyrighted works at a single online site are covered by a single notification, a representative list of such works at the Website;</p>
<p>3.     Identification of the material that is claimed to be infringing or to be the subject of infringing activity and that is to be removed or access to which is to be disabled, and information reasonably sufficient to permit <? echo $sitename; ?> to locate the material;</p>
<p>4.     Information reasonably sufficient to permit <? echo $sitename; ?> to contact the complaining party, including a name, address, telephone number and, if available, an email address at which the complaining party may be contacted;</p>
<p>5.     A statement that the complaining party has a good-faith belief that use of the material in the manner complained of is not authorized by the copyright owner, its agent or the law; and</p>
<p>6.     A statement that the information in the notification is accurate and, under penalty of perjury, that the complaining party is authorized to act on behalf of the owner of an exclusive right that is allegedly infringed.<br />
</p>
<p>All claims of copyright infringement on or regarding this Website should be delivered to <? echo $sitename; ?>'s designated copyright agent at the following address:</p>
<p>Copyright Contact Information:<br />
  Please contact us at our <a href='<? echo $basehttp; ?>/contact.php'>contact page</a>:</p>

<p>We apologize for any kind of misuse of our service and promise to do our best to find and terminate abusive files.<br />
</p>
</div>