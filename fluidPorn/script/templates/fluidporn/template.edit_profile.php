<div class="forms-wrapper">
    <div class="forms">
	
		<? if($message) { echo "<div class='notification success'><p>$message</p></div>"; } ?>

		<form action="" method="post" enctype="multipart/form-data" name="form1" id="form1">
		  <table border="0" align="left">
			<tr>
			  <td>Change Password</td>
			  <td><input type="text" name="newpassword" id="textfield" value='' /></td>
			</tr>
			<tr>
			  <td>Email Address</td>
			  <td><input type="text" name="email" id="textfield" value='<? echo $urow['email']; ?>' /></td>
			</tr>
			<tr>
			  <td>Location</td>
			  <td><input type="text" name="location" id="textfield" value='<? echo $urow['location']; ?>' /></td>
			</tr>
			<tr>
			  <td>Age</td>
			  <td><select name="age" id="select" style='width: 50px;'>
		   <? for($i = 18; $i < 100; $i++) {
				if($urow['age'] == $i) { $selected = 'selected'; } else { $selected = ''; }
				echo "<option $selected value='$i'> $i </option>";  
			  }
			  ?>
			  </select>			  </td>
			</tr>
			<tr>
			  <td>Gender</td>
			  <td><select name="gender" id="select2">
				<option <? if($urow['gender'] == 'Male') { echo 'selected'; } ?> value='Male'>Male</option>
				<option <? if($urow['gender'] == 'Female') { echo 'selected'; } ?> value='Female'>Female</option>
			  </select>			  </td>
			</tr>
			
		<?
		foreach($custom_user_fields as $k=>$v) {
		?>
		<tr>
			  <td><? echo $k; ?></td>
			  <td>
			  <? if(is_array($v)) { ?>
			  <select name="custom[<? echo $k; ?>]">
					<? foreach($v as $i) { 
						if($custom[$k] == $i) { $selected = 'selected'; } else { $selected = ''; }
					?>
					<option <? echo $selected; ?>><? echo $i; ?></option>
					<? } ?>
			  </select>
			  <? } else { ?>
			  <input type="text" name="custom[<? echo $k; ?>]" id="textfield" value='<? echo htmlentities($custom[$k]); ?>' />
			  <? } ?>			  </td>
			</tr>
		<? } ?>
			
			
			<tr>
			  <td valign="top">A Bit About Yourself...</td>
			  <td><textarea name="description" id="textarea" cols="45" rows="8"><? echo $urow['description']; ?></textarea></td>
			</tr>
			<tr>
				<td valign="top">&nbsp;</td>
				<td><input type="checkbox" name="inboxEmail" value='1' id="checkbox"  <? if($urow[inboxEmail] == 1) { echo "checked='checked'"; } ?> />
					Email me when someone sends me a private message</td>
			</tr>
			<tr>
				<td valign="top">&nbsp;</td>
				<td><input type="checkbox" name="friendsEmail" value='1' id="checkbox"  <? if($urow[friendsEmail] == 1) { echo "checked='checked'"; } ?> />
					Email me when someone sends me a friend request</td>
			</tr>            
			<tr>
				<td valign="top">&nbsp;</td>
				<td><input type="checkbox" name="wallPostEmail" value='1' id="checkbox2" <? if($urow[wallPostEmail] == 1) { echo "checked='checked'"; } ?> />
Email me when someone writes on my wall</td>
			</tr>
			<tr>
			  <td>Upload Avatar (<? echo $userThumbWidth; ?>x<? echo $userThumbHeight; ?>, max 50kb)</td>
			  <td><input type="file" name="file" id="fileField" /></td>
			</tr>
			<? if($urow['avatar'] != '') { ?>
			<tr>
			  <td>Current Avatar</td>
			  <td align='left'><img src='<? echo $misc_url; ?>/<? echo $urow['avatar']; ?>' width=170 height=130 /></td>
			</tr>
			<? } ?>
			<tr>
			  <td></td>
			  <td colspan="2"><input type="submit" name="button" id="button" value="Save" /></td>
			</tr>
		  </table>
		</form>
</div>
</div>