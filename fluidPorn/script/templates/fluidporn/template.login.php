<div class="forms-wrapper">
    <div class="forms">
	
        <? if($errors){ ?>
        <div class="notification error">
		<p>
            <?
                foreach($errors as $err){
                    echo $err.'<br />';
                }
            ?>
		</p>	
        </div>
        <? } else { ?>
        <div class="notification info">
		<p>
            You may login to your account using the form below.<br>
            <a href='<? echo $basehttp; ?>/signup'>Not a member? Click here to sign up, its free!</a>
        </p>
	    </div>	
		<? } ?>
		
        <form  name="loginForm" method="post" action="">
            <div class="form-label">Username</div>
            <div class="form-field"><input class="text" id="ahd_username" name="ahd_username" autocomplete='off' type="text" maxlength="255" style="width:300px;" ></div>
            <div class="form-label">Password</div>
            <div class="form-field"><input class="text" id="ahd_password" name="ahd_password" autocomplete='off' type="password" style="width:300px;" ></div>
            <div class="form-field"><a href="<? echo $basehttp; ?>/forgot-pass">Forgot Password?</a></div>
            <div class="form-field"><input class="submit" type="submit" name="Submit" id="button" value="Login" /></div>
            
			<? if(!$_SESSION[userid] && $enable_facebook_login){ ?>
            <?php include($basepath.'/facebook_login.php'); ?>					
                 <div class="form-field" style="margin-top:10px;  float:left;"><a href="<? echo $basehttp; ?>/includes/facebook/facebook.php"><img src="<?php echo $basehttp;?>/core/images/facebook-login-button.png" alt="Login by FaceBook" border="0" /></a></div>   
            <? } ?>    
			<? if(!$_SESSION[userid] && $enable_twitter_login){ ?>
            	<div class="form-field" style="margin-top:10px; float:left;">
                <a href="<?php echo $twitter->getAuthenticateUrl(); ?>&force_login=true"><img src="<?php echo $basehttp;?>/core/images/twitter-login-button.png" alt="Login by Twitter" /></a></div>
            <? } ?>       
            
        </form>
    </div>
</div>