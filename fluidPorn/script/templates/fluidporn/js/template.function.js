function changeSearch(id) {				
	$("#Search"+id).addClass('active');
	if(id=='Button1'){
 
		$("#type").val('videos');
		$("ul#search-list li.first").text('Videos');
	}
	if(id=='Button2'){
 
		$("#type").val('photos');
		$("ul#search-list li.first").text('Photos');
	}		
	if(id=='Button3'){
 
		$("#type").val('members');
		$("ul#search-list li.first").text('Members');
	}										
										
}	

$(function() {
	$("#main select").uniform();
});


/* ADDON NAV APP */
(function($){
    $.fn.addonNav = function(params){
        var $addons = $(this);
        var param = $.extend({
            hideRightMargin: '-75',
            showRightMargin: '-2',
            animateDuration: 600
        },params);
        
        $addons.find('ul').children().each(function(index){
            $(this).css({'zIndex' : 99 - index});
        });
        
        $addons.find('a').bind('mouseover mouseleave',function(e){
            if(e.type == 'mouseover'){
                $(this).stop(true,true).animate({'right' : param.showRightMargin + 'px'},param.animateDuration);
            }else{
                if($(this).hasClass('active')){}else{
                    $(this).animate({'right' : param.hideRightMargin + 'px'},param.animateDuration);
                }
            }
        });
    }
})(jQuery);



/*
$(document).ready(function(){
	if($(window).width() < 1350){
		$('#addons').remove();
	}
	

	$('#addons').addonNav({
		showRightMargin: '-2',
		hideRightMargin: '-75',
		animateDuration: 200
	});

	var menu = $('#addons'); 
	var menuPositionTop = menu.position().top; 
	var menuPositionTop = 120;
	$(window).scroll(function () {
		if(parseInt($(window).scrollTop()) > menuPositionTop) {	
			if (menu.hasClass('static')) {
				menu.removeClass('static').addClass('fixed'); 
			}
		}						
		else {
			if (menu.hasClass('fixed')) {
				menu.removeClass('fixed').addClass('static'); 
			}
		}
	});



});
*/

function bookmarksite(title,url){
if (window.sidebar) 
	window.sidebar.addPanel(title, url, "");
else if(window.opera && window.print){ 
	var elem = document.createElement('a');
	elem.setAttribute('href',url);
	elem.setAttribute('title',title);
	elem.setAttribute('rel','sidebar');
	elem.click();
} 
else if(document.all)
	window.external.AddFavorite(url, title);
}

//hazeSlider - simple and quick to implement jquery slider based on html list
(function($){
    $.fn.hazeSlider = function(options){
        var opts = $.extend({
            visible: 1,
            duration: 1000,
            showtime: 2000
        },options);

        //slider part
        var $slider = $(this);
        var width =  $slider.find('.list').children(':first').outerWidth();

        var max = $slider.find('.list').children().length;
        $slider.find('.list').css({ 'width' : (max * width) + 'px' });

        //if less elements then slider predicts - dont show slider
        if(max > opts.visible){
            var i = 0;
            var $btns = $('#left-arr,#right-arr');
            var elems = Math.round(max/opts.visible);
            $btns.each(function(){
                $(this).click(function(){
                    if($(this).attr('id') == 'left-arr'){
                        ++i;
                        if(i > 0) i = (0 - (elems - 1));
                    }else if($(this).attr('id') == 'right-arr'){
                        --i;
                        if(i < (0 - (elems - 1))) i = 0;
                    }
                    $slider.find('.list').animate({ 'marginLeft' : ((i * width) * opts.visible) + 'px' }, opts.duration);
                });
            });
        }
    };
})(jQuery);
 

(function($) {
	$.fn.fitsize = function(options) {
		var settings = $.extend({
			ratio: '1.3235'
		}, options);

		ratio = settings.ratio;

		this.each( function () {
			th_width = this.width ;
			th_height = th_width / ratio;
			$(this).css('height',th_height);
		}) ;


		return this;
	}
}(jQuery));

(function($) {
	$.fn.fitsize_video = function(options) {
		var settings = $.extend({
			ratio: '1.3235'
		}, options);

		ratio = settings.ratio;

		th_width = $(this).width() ;

		th_height = th_width / ratio;
		$(this).css('height',th_height);


		return this;
	}
}(jQuery));


$(document).ready(function() {
	var thumbs = $('.content img.content_image');
	var video = $('#player iframe') ;
    
    
	var ratiocalc_thumbs = (453 / 315);
	var ratiocalc_video = (890 / 505) ;

	function resize_all() {
		thumbs.fitsize({
			ratio: ratiocalc_thumbs
		});
		video.fitsize_video({
			ratio: ratiocalc_video
		});
        
	}

	$(window).load(function() {
		resize_all() ;
	});

	$(window).resize(function() {
		resize_all() ;
	})
});


 


 
 

