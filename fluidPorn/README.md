Thank you for purchasing this MechBunny 5.0 template.

It's installation is simple. Within the zip file you will find folders which you
need to upload to your server (into tube script root folder).

After the files are uploaded, you only need to slightly alter the config.php
file (which you can find in your 'admin' folder).


1. SET THE TEMPLATE FOLDERS:

Edit these two lines in your config.php related to templates so they look
like this:

$template_path = $basepath.'/templates/fluidporn';
$template_url = $basehttp.'/templates/fluidporn';


2. SET THE THUMB SIZES:

Since the template handles thumbs in a very special way, rescaling them as
the screen goes smaller or larger, it is advised to have the thumbs
generated in larger sizes. While we understand that it may have some impact
on the loading times of your website, we recommend the following values:

$thumbwidth = 453;
$thumbheight = 315;

$picthumbwidth = 453;
$picthumbheight = 315;



3. SET MOBILE AND ADDITIONAL VARIABLES:
Also in admin/config.php for this template additional variables should be changed:

$results_per_row = 4;
$enableMobile = false;


4. REPLACE YOUR LOGO

You can replace your logo by simply uploading a PNG file named logo.png to
the 'img' folder within the template folder.

Should you have any questions, do not hesitate to contact us at:
konrad@mechbunny.com
Skype: mechbunnymedia

Enjoy your new template,
MechBunny Team
